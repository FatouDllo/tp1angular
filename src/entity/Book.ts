export interface  Book {
    isbn : bigint 
    title : string 
    publisher : string  | null
    year: string | null
    
}