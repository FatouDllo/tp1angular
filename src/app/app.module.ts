import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { CoversComponent } from './covers/covers.component';
import { CoverComponent } from './cover/cover.component';
import { NewCoverComponent } from './new-cover/new-cover.component';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    CoversComponent,
    CoverComponent,
    NewCoverComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
