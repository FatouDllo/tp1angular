import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CoverComponent } from './cover/cover.component';
import { CoversComponent } from './covers/covers.component';
import { NewCoverComponent } from './new-cover/new-cover.component';



const routes: Routes = [
  {path:'new-cover/:id', component:NewCoverComponent},
  {path:'cover/:id', component:CoverComponent},
  {path:'new-cover', component:NewCoverComponent},
  { path: 'covers', component:CoversComponent}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
