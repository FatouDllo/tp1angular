import { Component, OnInit } from '@angular/core';
import { Book } from 'src/entity/Book';
import { BookService } from '../service/book.service';

@Component({
  selector: 'app-covers',
  templateUrl: './covers.component.html',
  styleUrls: ['./covers.component.scss']
})
export class CoversComponent implements OnInit {

  covers: Map<bigint, Book> = new Map()
  coversArray!: Array<Book>;

  constructor(public bookService : BookService) {

  }



  ngOnInit(): void {
    this.covers = this.bookService.getBooks(); 
    this.coversArray = Array.from(this.covers.values())
  }

  remove(id: bigint) {
    if (id !== undefined) {
      this.bookService.removeBook(id)
      this.coversArray = Array.from(this.bookService.getBooks().values())
    }
  }

}
