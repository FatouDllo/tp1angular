import { Injectable } from '@angular/core';
import { Book } from 'src/entity/Book';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  /*
  //dans le cas d'une map de nomnres 
  private books :  Map<number, Book> = new Map<number, Book> ([
    [1,{isbn: 1, title: 'Programmation fonctionnelle', publisher: "", year: null}],
    [2,{isbn: 2, title: 'Framework Web',  publisher: "", year: null} ],
    [3,{isbn: 3, title: 'Projet 1',  publisher: "", year: null}]
  ]); 

  constructor() { }

  getBooks() : Array<Book> {
    return Array.from(this.books.values()); 
  }

  */

  private books: Map<bigint, Book> = new Map().set(BigInt(1),
  {
    isbn: BigInt(1), title: 'Programmation fonctionnelle', 
    publisher: "tourelles-dauphines edition", year: 2020
  })
  .set(BigInt(2),
  {
    isbn: BigInt(2), title: 'Vue Js', 
    publisher: "la maison de leon", year: 2009})
  .set(BigInt(3),
  {
    isbn: BigInt(3), title: 'Framework Web',  
    publisher: "edition jean jacques", year: 2012
  });

  
  getBooks() : Map<bigint,Book> {
    return this.books ; 
  }



  createBook(book : Book) : void {
     const newId: bigint = BookService.maximum(this.books.keys()) + BigInt(1) ;
     let newBook: Book = {
       isbn: newId,
       title: book.title,
       publisher: book.publisher,
       year:book.year
     }

     console.log(newBook)

     this.books.set(newId, newBook);

  }

  public getBookById(id: bigint): Book | undefined {
    const book = this.books.get(id)
    if (book === null) {
      return undefined
    }
    return book 
  }

  public removeBook(id : bigint ): boolean {

      return this.books.delete(id)
  }

  private static maximum(array: IterableIterator<bigint>): bigint {
    let max : bigint = BigInt(0)
    for(let n of array)
      if(n>max) max = n
    return max
  }

  //l’obtention d’un tableau d’entités qui remplissent une certaine condition donnée sous la
  //forme d’un prédicate (par exemple tous les livres dont le titre contient un certain mot)

}
