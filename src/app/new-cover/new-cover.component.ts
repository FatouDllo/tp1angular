import { Component, OnInit } from '@angular/core';
import { Book } from 'src/entity/Book';
import { BookService } from '../service/book.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common'; 

@Component({
  selector: 'app-new-cover',
  templateUrl: './new-cover.component.html',
  styleUrls: ['./new-cover.component.scss']
})
export class NewCoverComponent implements OnInit {

  id : bigint | undefined = undefined
  cover: Book  = {
    isbn : BigInt(0), 
    title : " ",
    publisher : " ",
    year : " "
  }


  constructor(private bookService: BookService, 
    private route: ActivatedRoute) { }

  private set( title: string, publisher:string, year: string) : void {

    this.cover.title = title, 
    this.cover.publisher = publisher, 
    this.cover.year = year 
  }
  
  create(title: string, publisher:string, year: string) : void {
    this.set(title,publisher,year)
    this.bookService.createBook(this.cover)
    //this.goBack()
  }

  update() {

  }

  ngOnInit(): void {
    const idParameter: string | null = this.route.snapshot.paramMap.get('id')
    if (idParameter !== null) {
      this.id = BigInt(idParameter)
      const cover = this.bookService.getBookById(this.id)
      if (cover !== undefined)
        this.cover = cover
    }
  }

}
