import {Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Book } from 'src/entity/Book';
import { BookService } from '../service/book.service';

@Component({
  selector: 'app-cover',
  providers: [ Location ],
  templateUrl: './cover.component.html',
  styleUrls: ['./cover.component.scss']
})
export class CoverComponent implements OnInit {

  id : bigint | undefined = undefined 
  cover: Book | undefined = undefined

  constructor(private bookService: BookService, 
    private route: ActivatedRoute, 
    private location : Location) { }

  
   goBack() : void {
      this.location.back()
    }

  remove() {
    if (this.id !== undefined) {
      this.bookService.removeBook(this.id)
    }
    this.goBack()
  }

  ngOnInit(): void {

    const idParameter : string | null = this.route.snapshot.paramMap.get('id')
    if(idParameter!==null) {
      this.id = BigInt(idParameter)
      this.cover = this.bookService.getBookById(this.id)
    }

  }

 

}
